/*------------------------------------------------------------------------------
# Gulp Config
------------------------------------------------------------------------------*/

var themeName            = 'TB Social Share';
var themeDomain          = 'tb-social-share';
var WordPressURL         = 'http://localhost/theme/starter';
var translationPackage   = 'TB Social Share';
var translationBugReport = 'http://www.themesbros.com/contact/';
var translationTeam      = 'ThemesBros <support@themebros.com>';
var zipLocation          = '/home/sin2384/Desktop/themezip/' + themeName;

/*------------------------------------------------------------------------------
# Gulp Plugins
------------------------------------------------------------------------------*/

var gulp         = require('gulp'),
    browserSync  = require('browser-sync').create(),
    autoprefixer = require('gulp-autoprefixer'),
    concat       = require('gulp-concat'),
    cssmin       = require('gulp-cssmin'),
    imagemin     = require('gulp-imagemin'),
    //jshint     = require( 'gulp-jshint' ),
    rename       = require('gulp-rename'),
    rtlcss       = require('gulp-rtlcss'),
    sass         = require('gulp-sass'),
    sort         = require('gulp-sort'),
    uglify       = require('gulp-uglify'),
    copy         = require('gulp-copy'),
    wppot        = require('gulp-wp-pot'),
    zip          = require('gulp-zip'),
    replace      = require('gulp-replace'),
    postcss      = require('gulp-postcss'),
    cleanCSS     = require('gulp-clean-css'),
    pngquant     = require('imagemin-pngquant'),
    gcmq         = require('gulp-group-css-media-queries'),
	gsgc         = require('gulp-sass-generate-contents');

/*------------------------------------------------------------------------------
# AUTOPREFIXER
------------------------------------------------------------------------------*/

// Browsers you care about for autoprefixing.
// Browserlist https://github.com/ai/browserslist
const AUTOPREFIXER_BROWSERS = [
    'last 2 version',
    '> 1%',
    'ie >= 9',
    'ie_mob >= 10',
    'ff >= 30',
    'chrome >= 34',
    'safari >= 7',
    'opera >= 23',
    'ios >= 7',
    'android >= 4',
    'bb >= 10'
];

/*------------------------------------------------------------------------------
# Gulp Tasks
------------------------------------------------------------------------------*/

gulp.task('styles', function () {
    gulp.src(['assets/css/*.css', '!**/*.min.css', 'style.css'], {
            base: '.'
        })
        .pipe(cssmin())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('./'))
});

gulp.task('rtlcss', function () {
    gulp.src(['style.css'])
        .pipe(postcss([
            require('postcss-wprtl')
        ]))
        // .pipe(cleanCSS({debug: true}, function(details) {
        // console.log(details.name + ': ' + details.stats.originalSize);
        // console.log(details.name + ': ' + details.stats.minifiedSize);
        // }))
        .pipe(rename({
            basename: 'rtl'
        }))
        .pipe(gulp.dest('./'))
});

gulp.task('scripts', function () {
    gulp.src(['assets/js/*.js', '!assets/js/*.min.js'])
        .pipe(uglify())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('assets/js/'))
});

gulp.task('imagemin', function () {
    gulp.src('images/**')
        .pipe(
            imagemin({
                progressive: true,
                interlaced: true,
                svgoPlugins: [{
                        removeViewBox: false
                    },
                    {
                        cleanupIDs: false
                    }
                ],
                use: [pngquant()]
            })
        )
        .pipe(gulp.dest('images'))
});

gulp.task('pot', function () {
    gulp.src('**/*.php')
        .pipe(wppot({
            domain: themeDomain,
            package: translationPackage,
            bugReport: translationBugReport,
            team: translationTeam,
            headers: {
                Language: 'en_US'
            }
        }))
        .pipe(gulp.dest('languages/' + themeDomain + '.pot'));
});

gulp.task('sass', function () {
    gulp.src('scss/**/**.scss')
        .pipe(sass({
            outputStyle: 'expanded'
        }).on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: AUTOPREFIXER_BROWSERS,
        }))
        .pipe(gcmq())
        .pipe(gulp.dest('./assets/css/'))
        .pipe(browserSync.stream());
});

gulp.task('serve', function () {

    browserSync.init({
        proxy: WordPressURL,
        notify: false,
        injectChanges: true,
        ghostMode: {
            forms: false,
        }
        // Tunnel the Browsersync server through a random Public URL
        // -> http://randomstring23232.localtunnel.me
        //tunnel: true,
        // Attempt to use the URL "http://my-private-site.localtunnel.me"
        //tunnel: "pextestera23",
    });

    gulp.watch("scss/**/**.scss", ['sass', 'styles']);

    gulp.watch('style.css', function () {
        gulp.src('style.css')
            .pipe(browserSync.stream())
            .pipe(cssmin())
            .pipe(rename({
                suffix: '.min'
            }))
            .pipe(gulp.dest('./'))
    });

    gulp.watch("**/*.php").on('change', browserSync.reload);
    //gulp.watch("**/*.css").on('change', browserSync.reload);
    gulp.watch("**/*.js").on('change', browserSync.reload);
});

gulp.task('watch', function () {
    gulp.watch('*.css', ['serve']);
});

gulp.task('zip', () => {
    return gulp.src(['**/*', '!*sublime*', '!{.git,.git/**,scss,scss/**,node_modules,node_modules/**}', '!gulpfile.js', '!*.json', '!*todo'])
        .pipe(zip(themeDomain + '.zip'))
        .pipe(gulp.dest(zipLocation));
});

