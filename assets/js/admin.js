(function( $ ){
	'use strict';
	$( '.sortable' ).sortable();

	$( '.tbss-style' ).on(
		'change',
		function(){
			check_selected( $( this ) );
		}
	);

	function check_selected( tbss_select ) {
		if ( '' !== tbss_select ) {
			tbss_select = $( '.tbss-style' ).val();
		}

		$( '.form-table tr' ).show();

		switch (tbss_select) {
			case 'fullwidth':
				$( '.form-table tr' ).eq( 4 ).hide();
				$( '.form-table tr' ).eq( 5 ).hide();
				$( '.form-table tr' ).eq( 6 ).hide();
				break;
			case 'icon':
				$( '.form-table tr' ).eq( 5 ).hide();
				$( '.form-table tr' ).eq( 6 ).hide();
				break;
			case 'icon-text':
				$( '.form-table tr' ).eq( 4 ).hide();
				$( '.form-table tr' ).eq( 6 ).hide();
				break;
			default:
				$( '.form-table tr' ).eq( 4 ).hide();
				$( '.form-table tr' ).eq( 5 ).hide();
		}
	}

	check_selected();
})( jQuery );
