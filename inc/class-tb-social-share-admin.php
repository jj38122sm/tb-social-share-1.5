<?php
/**
 * Plugin options.
 *
 * @package   TB Social Share
 * @version   1.0.0
 * @author    ThemesBros
 * @copyright Copyright (c) 2011 - 2017, ThemesBros
 */

/* If this file is called directly, abort. */
if ( ! defined( 'ABSPATH' ) ) {
	die;
}

/**
 * Plugin admin class.
 *
 * @since 1.0.0
 */
class TB_Social_Share_Admin {

	/**
	 * Sets up needed actions for the admin to initialize.
	 *
	 * @since  1.0.0
	 * @return void
	 */
	public function __construct() {
		add_action( 'admin_menu', array( $this, 'add_admin_menu' ) );
		add_action( 'admin_init', array( $this, 'settings_init' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue' ) );
	}

	/**
	 * List of supported share sites.
	 *
	 * @since  1.0.0
	 * @return array
	 */
	public function get_site_list() {
		return array(
			'facebook'  => __( 'Facebook', 'tb-social-share' ),
			'twitter'   => __( 'Twitter', 'tb-social-share' ),
			'gplus'     => __( 'Google Plus', 'tb-social-share' ),
			'pinterest' => __( 'Pinterest', 'tb-social-share' ),
			'linkedin'  => __( 'Linkedin', 'tb-social-share' ),
			'reddit'    => __( 'Reddit', 'tb-social-share' ),
			'tumblr'    => __( 'Tumblr', 'tb-social-share' ),
			'vk'        => __( 'Vk', 'tb-social-share' ),
			'email'     => __( 'Email', 'tb-social-share' ),
		);
	}

	/**
	 * Adds link to admin menu under "Settings".
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function add_admin_menu() {
		add_submenu_page(
			'options-general.php',
			esc_html__( 'TB Social Share', 'tb-social-share' ),
			esc_html__( 'TB Social Share', 'tb-social-share' ),
			'manage_options',
			'tb_social_share',
			array( $this, 'display_options' )
		);
	}

	/**
	 * Initialize settings API to create plugin options.
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function settings_init() {

		register_setting( 'tbss_options', 'tbss_settings', array( $this, 'sanitize_data' ) );

		add_settings_section(
			'tbss_section',
			esc_html__( 'TB Social Share Options', 'tb-social-share' ),
			array( $this, 'settings_section_display' ),
			'tbss_options'
		);

		add_settings_field(
			'status',
			esc_html__( 'Status', 'tb-social-share' ),
			array( $this, 'display_status' ),
			'tbss_options',
			'tbss_section'
		);

		add_settings_field(
			'sites',
			esc_html__( 'Sites', 'tb-social-share' ),
			array( $this, 'display_sites' ),
			'tbss_options',
			'tbss_section'
		);

		add_settings_field(
			'position',
			esc_html__( 'Position', 'tb-social-share' ),
			array( $this, 'display_position' ),
			'tbss_options',
			'tbss_section'
		);

		add_settings_field(
			'styles',
			esc_html__( 'Select style', 'tb-social-share' ),
			array( $this, 'display_styles' ),
			'tbss_options',
			'tbss_section'
		);

		add_settings_field(
			'icon',
			esc_html__( 'Icon style settings', 'tb-social-share' ),
			array( $this, 'display_icon_style_settings' ),
			'tbss_options',
			'tbss_section'
		);

		add_settings_field(
			'icon-text',
			esc_html__( 'Icon text style settings', 'tb-social-share' ),
			array( $this, 'display_icon_text_settings' ),
			'tbss_options',
			'tbss_section'
		);

		add_settings_field(
			'mix',
			esc_html__( 'Icon mix style settings', 'tb-social-share' ),
			array( $this, 'display_icon_mix_settings' ),
			'tbss_options',
			'tbss_section'
		);

	}

	/**
	 * Adds required scripts for plugin functionality (jQuery, jQuery-ui-sortable).
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function enqueue() {

		// Load scripts only on plugin page.
		if ( 'tb_social_share' != isset( $_GET['page'] ) ) {
			return;
		}

		// Use minified files if SCRIPT_DEBUG is off.
		$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

		wp_enqueue_script( 'jquery-ui-sortable' );

		wp_enqueue_script(
			'tbss-admin',
			trailingslashit( plugins_url( '../assets/js/', __FILE__ ) ) . "admin{$suffix}.js",
			array( 'jquery' ),
			'1.0.0',
			true
		);

	}

	/**
	 * Callback: displays section info.
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function settings_section_display() {
		printf( '<p>%s</p>', esc_html__( 'Customize plugin behaviour.', 'tb-social-share' ) );
	}

	/**
	 * Callback: displays checkbox.
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function display_status() {
		$options = get_option( 'tbss_settings' );
		?>
		<label for="status"><input type="checkbox" id="status" name="tbss_settings[status]" <?php isset( $options['status'] ) ? checked( $options['status'], 1 ) : ''; ?>> <?php esc_html_e( 'Check to enable', 'tb-social-share' ); ?></label>
		<?php
	}


	/**
	 * Callback: displays checkboxes with sites.
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function display_sites() {

		$options = get_option( 'tbss_settings' );

		$sites = isset( $options['site'] ) ? $this->get_sorted_sites( $options['site'] ) : $this->get_site_list();

		echo '<ul class="sortable">';

		foreach ( $sites as $id => $name ) {
			printf(
				'<li><span class="dashicons dashicons-move"></span> <label for="site-%1$s"><input id="site-%1$s" type="checkbox" name="tbss_settings[site][%1$s]"%2$s>%3$s</label></li>',
				esc_attr( $id ),
				! empty( $options['site'][ $id ] ) ? sprintf( ' %s', checked( 1, 1, false ) ) : '',
				esc_html( $name )
			);
		}

		echo '</ul>';

	}

	/**
	 * Callback: displays select.
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function display_position() {
		$options = get_option( 'tbss_settings' );
		?>
		<select name="tbss_settings[position]">
			<option value="custom" <?php selected( $options['position'], 'custom' ); ?>><?php esc_html_e( 'Custom position', 'tb-social-share' ); ?></option>
			<option value="before" <?php selected( $options['position'], 'before' ); ?>><?php esc_html_e( 'Above the content', 'tb-social-share' ); ?></option>
			<option value="after" <?php selected( $options['position'], 'after' ); ?>><?php esc_html_e( 'Below the content', 'tb-social-share' ); ?></option>
		</select>
		<p><?php esc_html_e( 'If custom position is chosen, you can place function in your theme like this:', 'tb-social-share' ); ?>
		<br>
		<code>
			&lt;?php if ( function_exists( 'tb_social_share_display' ) ) { echo tb_social_share_display(); } ?&gt;
		</code>
		</p>
		<?php
	}

	/**
	 * Callback: displays styles.
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function display_styles() {
		$options = get_option( 'tbss_settings' );
		?>

		<select class="tbss-style" name="tbss_settings[style]">
			<option value="fullwidth" <?php selected( $options['style'], 'fullwidth' ); ?>><?php esc_html_e( 'Fullwidth', 'tb-social-share' ); ?></option>
			<option value="icon" <?php selected( $options['style'], 'icon' ); ?>><?php esc_html_e( 'Icon only', 'tb-social-share' ); ?></option>
			<option value="icon-text" <?php selected( $options['style'], 'icon-text' ); ?>><?php esc_html_e( 'Icon and text', 'tb-social-share' ); ?></option>
			<option value="icon-mix" <?php selected( $options['style'], 'icon-mix' ); ?>><?php esc_html_e( 'Mixed icon(s) and text with icons', 'tb-social-share' ); ?></option>
		</select>
		<?php
	}

	/**
	 * Callback: displays icon style settings.
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function display_icon_style_settings() {
		$options = get_option( 'tbss_settings' );
		if ( ! isset( $options['icon-shape'] ) ) {
			$options['icon-shape'] = 'square';
		}
		if ( ! isset( $options['icon-color'] ) ) {
			$options['icon-color'] = 'native';
		}
		?>
		<div class="icon-style-settings">
			<strong><?php esc_html_e( 'Shape:', 'tb-social-share' ); ?></strong><br><br>
			<input id="shape-icon-1" type="radio" value="square" name="tbss_settings[icon-shape]" <?php checked( $options['icon-shape'], 'square' ); ?>>
			<label for="shape-icon-1">
				<?php esc_html_e( 'Square', 'tb-social-share' ); ?>
			</label>
			<input id="shape-icon-2" type="radio" value="rounded" name="tbss_settings[icon-shape]" <?php checked( $options['icon-shape'], 'rounded' ); ?>>
			<label for="shape-icon-2">
				<?php esc_html_e( 'Rounded', 'tb-social-share' ); ?>
			</label>
			<input id="shape-icon-3" type="radio" value="circle" name="tbss_settings[icon-shape]" <?php checked( $options['icon-shape'], 'circle' ); ?>>
			<label for="shape-icon-3">
				<?php esc_html_e( 'Circle', 'tb-social-share' ); ?>
			</label>
			<br>
			<br>

			<strong><?php esc_html_e( 'Color:', 'tb-social-share' ); ?></strong><br><br>
			<input id="color-icon-1" type="radio" value="native" name="tbss_settings[icon-color]" <?php checked( $options['icon-color'], 'native' ); ?>>
			<label for="color-icon-1">
				<?php esc_html_e( 'Native', 'tb-social-share' ); ?>
			</label>
			<input id="color-icon-2" type="radio" value="light" name="tbss_settings[icon-color]" <?php checked( $options['icon-color'], 'light' ); ?>>
			<label for="color-icon-2">
				<?php esc_html_e( 'Light', 'tb-social-share' ); ?>
			</label>
			<input id="color-icon-3" type="radio" value="dark" name="tbss_settings[icon-color]" <?php checked( $options['icon-color'], 'dark' ); ?>>
			<label for="color-icon-3">
				<?php esc_html_e( 'Dark', 'tb-social-share' ); ?>
			</label>
		</div>

		<?php
	}

	/**
	 * Callback: displays icon text style settings.
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function display_icon_text_settings() {
		$options = get_option( 'tbss_settings' );
		if ( ! isset( $options['icon-text-shape'] ) ) {
			$options['icon-text-shape'] = 'square';
		}
		if ( ! isset( $options['icon-text-color'] ) ) {
			$options['icon-text-color'] = 'native';
		}
		?>

		<div class="icon-text-style-settings">
			<strong><?php esc_html_e( 'Shape:', 'tb-social-share' ); ?></strong><br><br>
			<input id="shape-icon-text-1" type="radio" value="square" name="tbss_settings[icon-text-shape]" <?php checked( $options['icon-text-shape'], 'square' ); ?>>
			<label for="shape-icon-text-1">
				<?php esc_html_e( 'Square', 'tb-social-share' ); ?>
			</label>
			<input id="shape-icon-text-2" type="radio" value="rounded" name="tbss_settings[icon-text-shape]" <?php checked( $options['icon-text-shape'], 'rounded' ); ?>>
			<label for="shape-icon-text-2">
				<?php esc_html_e( 'Rounded', 'tb-social-share' ); ?>
			</label>
			<br>
			<br>

			<strong><?php esc_html_e( 'Color:', 'tb-social-share' ); ?></strong><br><br>
			<input id="color-icon-text-1" type="radio" value="native" name="tbss_settings[icon-text-color]" <?php checked( $options['icon-text-color'], 'native' ); ?>>
			<label for="color-icon-text-1">
				<?php esc_html_e( 'Native', 'tb-social-share' ); ?>
			</label>
			<input id="color-icon-text-2" type="radio" value="light" name="tbss_settings[icon-text-color]" <?php checked( $options['icon-text-color'], 'light' ); ?>>
			<label for="color-icon-text-2">
				<?php esc_html_e( 'Light', 'tb-social-share' ); ?>
			</label>
			<input id="color-icon-text-3" type="radio" value="dark" name="tbss_settings[icon-text-color]" <?php checked( $options['icon-text-color'], 'dark' ); ?>>
			<label for="color-icon-text-3">
				<?php esc_html_e( 'Dark', 'tb-social-share' ); ?>
			</label>
		</div>

		<?php
	}

	/**
	 * Callback: displays icon style settings.
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function display_icon_mix_settings() {
		$options = get_option( 'tbss_settings' );
		if ( ! isset( $options['icon-mix-shape'] ) ) {
			$options['icon-mix-shape'] = 'square';
		}
		if ( ! isset( $options['icon-mix-color'] ) ) {
			$options['icon-mix-color'] = 'native';
		}
		if ( ! isset( $options['mix-count'] ) ) {
			$options['mix-count'] = 1;
		}
		?>

		<div class="icon-mix-style-settings">
			<strong><?php esc_html_e( 'Shape:', 'tb-social-share' ); ?></strong><br><br>
			<input id="shape-mix-1" type="radio" value="square" name="tbss_settings[icon-mix-shape]" <?php checked( $options['icon-mix-shape'], 'square' ); ?>>
			<label for="shape-mix-1">
				<?php esc_html_e( 'Square', 'tb-social-share' ); ?>
			</label>
			<input id="shape-mix-2" type="radio" value="rounded" name="tbss_settings[icon-mix-shape]" <?php checked( $options['icon-mix-shape'], 'rounded' ); ?>>
			<label for="shape-mix-2">
				<?php esc_html_e( 'Rounded', 'tb-social-share' ); ?>
			</label>
			<br>
			<br>

			<strong><?php esc_html_e( 'Color:', 'tb-social-share' ); ?></strong><br><br>
			<input id="color-mix-1" type="radio" value="native" name="tbss_settings[icon-mix-color]" <?php checked( $options['icon-mix-color'], 'native' ); ?>>
			<label for="color-mix-1">
				<?php esc_html_e( 'Native', 'tb-social-share' ); ?>
			</label>
			<input id="color-mix-2" type="radio" value="light" name="tbss_settings[icon-mix-color]" <?php checked( $options['icon-mix-color'], 'light' ); ?>>
			<label for="color-mix-2">
				<?php esc_html_e( 'Light', 'tb-social-share' ); ?>
			</label>
			<input id="color-mix-3" type="radio" value="dark" name="tbss_settings[icon-mix-color]" <?php checked( $options['icon-mix-color'], 'dark' ); ?>>
			<label for="color-mix-3">
				<?php esc_html_e( 'Dark', 'tb-social-share' ); ?>
			</label>

			<br>
			<br>
			<label for="mix-count"><?php esc_html_e( 'Number of items with text:', 'tb-social-share' ); ?></label> &nbsp;
			<select id="mix-count" name="tbss_settings[mix-count]">
			<?php $count = count( $this->get_site_list() ); ?>
			<?php for ( $i = 1; $i <= $count; $i++ ) : ?>
				<option value="<?php echo absint( $i ); ?>" <?php echo selected( $i, $options['mix-count'] ); ?>><?php echo absint( $i ); ?></option>
			<?php endfor; ?>
		</select>
		</div>

		<?php
	}

	/**
	 * Callback: displays options page.
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function display_options() {
		?>
		<div class="wrap">
			<form action="options.php" method="POST">
				<?php
				settings_fields( 'tbss_options' );
				do_settings_sections( 'tbss_options' );
				submit_button();
				?>
			</form>
		</div>
		<?php
	}

	/**
	 * Callback: checks and validates user submitted data.
	 *
	 * @since 1.0.0
	 *
	 * @param array $input Array of data.
	 * @return array
	 */
	public function sanitize_data( $input ) {
		$output['status']          = isset( $input['status'] ) ? 1 : '';
		$output['position']        = sanitize_text_field( $input['position'] );
		$output['style']           = sanitize_text_field( $input['style'] );
		$output['icon-shape']      = sanitize_text_field( $input['icon-shape'] );
		$output['icon-color']      = sanitize_text_field( $input['icon-color'] );
		$output['icon-text-shape'] = sanitize_text_field( $input['icon-text-shape'] );
		$output['icon-text-color'] = sanitize_text_field( $input['icon-text-color'] );
		$output['icon-mix-shape']  = sanitize_text_field( $input['icon-mix-shape'] );
		$output['icon-mix-color']  = sanitize_text_field( $input['icon-mix-color'] );
		$output['mix-count']       = absint( $input['mix-count'] );

		$sites = $this->get_sorted_sites( $input['site'] );

		foreach ( $sites as $id => $name ) {
			$output['site'][ sanitize_text_field( $id ) ] = isset( $input['site'][ $id ] ) ? 1 : '';
		}

		return $output;
	}

	/**
	 * Set's new order of social share sites.
	 *
	 * @since 1.0.0
	 * @param array $order List of websites.
	 * @return array
	 */
	public function get_sorted_sites( $order ) {
		$site_list = $this->get_site_list();
		$new_order = array();
		foreach ( array_keys( $order ) as $key ) {
			$new_order[ $key ] = $site_list[ $key ];
		}
		return array_merge( $new_order, $site_list );
	}

}

new TB_Social_Share_Admin();
